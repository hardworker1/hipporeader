import requests
from lxml import etree
import re
from swagger_server.constants import *
import time
import uuid
from swagger_server.utils.oms_client import OmsClient
import ast
import json


def import_oms(data_key, namespace, data_value):
    try:
        oms_client = OmsClient(OMS_RETRIEVE_URL, namespace)
        data = oms_client.get_first_value(data_key)
    except:
        return False
    if not data:
        try:
            oms_client = OmsClient(OMS_CREATE_URL, namespace)
            oms_client.create(data_key, data_value)
        except:
            return False
    else:
        if data.get("dataValue").get('bookId'):
            book_id_list = data.get("dataValue").get('bookId')
            if book_id not in book_id_list:
                book_id_list.append(data_value.get("bookId"))
                data_value["bookId"] = book_id_list
        try:
            oms_client = OmsClient(OMS_UPDATE_URL, namespace)
            oms_client.update(data_key, data_value)
        except:
            return False
    return True


def import_oms_v2(data_key, namespace, data_value):
    oms_client = OmsClient(OMS_CREATE_URL, namespace)
    try:
        oms_client.create(data_key, data_value)
    except:
        return False
    return True


def get_data(book_id, signature, book_img):
    url = 'https://writer.muyewx.com/page/' + book_id
    headers = {
        'cookie': 'passport_csrf_token=e2db065d73c5cf5f7e19d32ce8fd2edd; passport_csrf_token_default=e2db065d73c5cf5f7e19d32ce8fd2edd; passport_auth_status_ss=a2e4969df7b539700760d2d9532f4626,3a1a54885b072906e780c23da43cd775; sid_guard=03fa57b10eddabd0ea74ea5458d4e9c5|1616481800|5183999|Sat,+22-May-2021+06:43:19+GMT; uid_tt=c3e80c73511fbdda0adbbe5ab943029a; uid_tt_ss=c3e80c73511fbdda0adbbe5ab943029a; sid_tt=03fa57b10eddabd0ea74ea5458d4e9c5; sessionid=03fa57b10eddabd0ea74ea5458d4e9c5; sessionid_ss=03fa57b10eddabd0ea74ea5458d4e9c5; passport_auth_status=a2e4969df7b539700760d2d9532f4626,3a1a54885b072906e780c23da43cd775; gftoken=MDNmYTU3YjEwZXwxNjE2NDgxODAwMTN8fDAGBgYGBgY; MONITOR_WEB_ID=d7985c30-5dff-458e-9c87-91fe8fa85e4f',
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.190 Safari/537.36'
    }
    response = requests.get(url, headers=headers)
    text = response.text
    html = etree.HTML(text)
    book_name = str(html.xpath('//div[@class="info-name"]/text()')[0])
    book_status = str(html.xpath('//span[@class="info-label-yellow"]/text()'))
    book_tags = str(html.xpath('//span[@class="info-label-grey"]/text()'))
    author_img = str(html.xpath('//img[@class="author-img"]/@src')[0])
    word_count = int(re.search('"wordNumber":(\d+),', text).group(1))
    reader_count = int(re.search('"readCount":(\d+),', text).group(1))
    author_name = re.search('"authorName":"(\w+)",', text, flags=0).group(1)
    author_description = re.search('"description":"((\w|[^\x00-\xff])+)",', text)
    if author_description:
        author_description = author_description.group(1)
    book_abstract = str(html.xpath('//div[@class="page-abstract-content"]/p/text()')[0])
    lastChapterTitle = str(html.xpath('//span[@class="info-last-title"]/text()')[1])
    lastPublishTime = re.search('"lastPublishTime":"(\d+)",', text).group(1)
    lastPublishTime = time.localtime(int(lastPublishTime))
    lastPublishTime = time.strftime("%Y-%m-%d %H:%M:%S", lastPublishTime)
    directory_url = 'https://writer.muyewx.com/api/reader/directory/detail?bookId=' + book_id + '&_signature=' + signature
    author_id = str(uuid.uuid4())
    author_data_value = {
        'userId': author_id,
        'userName': author_name,
        'image': author_img,
        'description': author_description,
        'bookId': [book_id]
    }
    status = import_oms(author_id, OMS_NAMESPACE_USER_PROFILE, author_data_value)
    if not status:
        print("import oms error, authorId={}".format(author_id))
        return False
    time.sleep(1)
    response = requests.get(directory_url)
    a = response.content.decode(encoding='utf8')
    response_json = json.loads(response.content.decode(encoding='utf8'))
    chapterListWithVolume = response_json.get('data').get('chapterListWithVolume')
    directory_info = []
    for item in chapterListWithVolume:
        directory_info.extend(item)
    book_tags = ast.literal_eval(book_tags)
    data_key = author_id + "#" + book_id
    new_book_tags = []
    for tag in book_tags:
        if tag in BOOK_TAGS_VALUE:
            tag_value = BOOK_TAGS_VALUE[tag]
            new_book_tags.append(tag_value)
            data_key = data_key + "#" + str(tag_value)
    book_status = ast.literal_eval(book_status)[0]
    book_status = BOOK_STATUS_VALUE[book_status]
    book_data_value = {
        "authorId": author_id,
        "authorName": author_name,
        "bookId": book_id,
        "bookName": book_name,
        "bookImg": book_img,
        "bookStatus": book_status,
        "bookTags": new_book_tags,
        "wordCount": word_count,
        "readerCount": reader_count,
        "bookAbstract": book_abstract,
        "directoryInfo": directory_info,
        "lastChapterTitle": lastChapterTitle,
        "lastPublishTime": lastPublishTime
    }
    status = import_oms_v2(data_key, OMS_NAMESPACE_BOOK_PROFILE, book_data_value)
    if not status:
        print("import oms error, dataKey={}".format(data_key))
        return False
    headers = {
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36'
    }
    i = 0
    for item in directory_info:
        item_id = item.get('itemId')
        url = 'https://writer.muyewx.com/reader/' + item_id
        response = requests.get(url, headers=headers)
        if response.status_code != 200:
            print("request error, itemId={}".format(item_id))
        text = response.text
        content_word_count = int(re.search('"chapterWordNumber":"(\d+)",', text).group(1))
        content_update_time = re.search('"firstPassTime":"(\d+)",', text).group(1)
        content_update_time = time.localtime(int(content_update_time))
        content_update_time = time.strftime("%Y-%m-%d %H:%M:%S", content_update_time)
        html = etree.HTML(text)
        content_list = html.xpath("//div[@class='muye-reader-content noselect']/div/p/text()")
        if len(content_list) > 0:
            content_list[0] = '<p>&emsp;' + content_list[0]
            content_list[-1] = content_list[-1] + "</p>"
            content_str = "</p><p>&emsp;".join(content_list)
            data_value = {
                "itemId": item_id,
                "contentWordCount": content_word_count,
                "contentUpdateTime": content_update_time,
                "content": content_str
            }
            status = import_oms(item_id, OMS_NAMESPACE_BOOK_CONTENT, data_value)
            if not status:
                print("import oms error, itemId={}".format(item_id))
                continue
            i += 1
            print(i)
            time.sleep(0.5)

if __name__ == '__main__':
    book_id = '6823204183357787143'
    signature = 'W-CcbAAgEA-33OdeYObj9FvgnHAADto'
    book_img = 'https://p3-tt.byteimg.com/img/ff040001f047037caff1~180x234.jpg'
    get_data(book_id, signature, book_img)