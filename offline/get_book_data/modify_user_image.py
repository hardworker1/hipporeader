from swagger_server.utils.oms_client import OmsClient
from swagger_server.constants import *

def update_book(userId, userImg):
    oms_client = OmsClient(OMS_RETRIEVE_URL, OMS_NAMESPACE_USER_PROFILE)
    try:
        result = oms_client.get_first_value('regex::.*{}.*'.format(userId))
    except:
        return 'error'
    data_value = result.get('dataValue')
    data_value['image'] = userImg
    data_key = result.get('dataKey')
    oms_client = OmsClient(OMS_UPDATE_URL, OMS_NAMESPACE_USER_PROFILE)
    try:
        oms_client.update(data_key, data_value)
    except:
        return 'error'
    return 'success'

if __name__ == '__main__':
    print(update_book('a87caac1-f14d-4372-9b3e-4bb837902f4d', 'https://ss0.baidu.com/94o3dSag_xI4khGko9WTAnF6hhy/zhidao/wh%3D450%2C600/sign=b9703c8cad014c08196e20a13f4b2e3e/1ad5ad6eddc451dad19fbdceb2fd5266d01632ad.jpg'))