from swagger_server.utils.oms_client import OmsClient
from swagger_server.constants import *

def update_book(bookId, bookImg):
    oms_client = OmsClient(OMS_RETRIEVE_URL, OMS_NAMESPACE_BOOK_PROFILE)
    try:
        result = oms_client.get_first_value('regex::.*{}.*'.format(bookId))
    except:
        return 'error'
    data_value = result.get('dataValue')
    data_value['bookImg'] = bookImg
    data_key = result.get('dataKey')
    oms_client = OmsClient(OMS_UPDATE_URL, OMS_NAMESPACE_BOOK_PROFILE)
    try:
        oms_client.update(data_key, data_value)
    except:
        return 'error'
    return 'success'

if __name__ == '__main__':
    print(update_book('2699742409972098062', 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=4085419271,2701690942&fm=11&gp=0.jpg'))