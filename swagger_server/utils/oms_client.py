from swagger_server.constants import *
import requests
import json


class OmsClient:
    def __init__(self, url, namespace):
        self.url = url
        self.namespace = namespace
        self.authorization = {'Authorization': OMS_AUTHORIZATION}

    def get_first_value(self, data_key):
        items = self.get_values(data_key)
        return items[0] if len(items) > 0 else None

    def get_values(self, data_key):
        params = {'dataKey': data_key, 'namespace': self.namespace}
        response = requests.get(self.url, params=params, headers=self.authorization)
        if response.status_code != 200:
            raise Exception('Request for data failed from oms')
        response_json = response.json()
        response_data = response_json.get('data')
        return response_data.get("items")

    def get_values_with_pagination(self, data_key, page_from=0, page_size=10, required_total_size=False,
                                   field_sort=None):
        params = {'dataKey': data_key, 'namespace': self.namespace, 'pageFrom': page_from, 'pageSize': page_size}
        if field_sort:
            params['filedSort'] = field_sort
        if required_total_size:
            params['requiredTotalSize'] = required_total_size
        response = requests.get(self.url, params=params, headers=self.authorization)
        if response.status_code != 200:
            raise Exception('Request for data failed from oms')
        response_json = response.json()
        response_data = response_json.get('data')
        return response_data

    def get_all_values(self, data_key, required_total_size=False, field_sort=None):
        all_data = {}
        data_items = []
        page_from = 0
        page_size = 100
        while True:
            response_data = self.get_values_with_pagination(data_key, page_from,
                                                            page_size,
                                                            required_total_size=True,
                                                            field_sort=field_sort)
            total_size = response_data.get('totalSize')
            data_items.extend(response_data.get('items'))
            all_data['totalSize'] = total_size
            if page_size + page_from > total_size:
                break
            page_from += page_size
        all_data['items'] = data_items
        return all_data

    def create(self, data_key, data_value):
        data = {
            'apiVersion': '1.0',
            'data': {
                'dataKey': data_key,
                'dataValue': data_value,
                'namespace': self.namespace
            }
        }
        response = requests.post(self.url,
                                 headers={**self.authorization, 'Content-Type': 'application/json'},
                                 data=json.dumps(data))
        if response.status_code != 200:
            raise Exception('Request for data failed from oms')

    def delete(self, data_key):
        params = {'dataKey': data_key, 'namespace': self.namespace}
        response = requests.delete(self.url, headers=self.authorization, params=params)
        if response.status_code != 200:
            raise Exception('Request for data failed from oms')
        return response.json().get('data')

    def update(self, data_key, data_value):
        url = self.url + "?namespace={}".format(self.namespace)
        response = requests.patch(url, headers={**self.authorization, 'Content-Type': 'application/json'},
                                  data=json.dumps({
                                      'apiVersion': '1.0',
                                      'data': {
                                          'dataKey': data_key,
                                          'dataValue': data_value
                                      }
                                  }))
        if response.status_code != 200:
            raise Exception('Request for data failed from oms')
        return response.json().get('data')
