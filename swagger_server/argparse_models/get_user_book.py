import argparse

get_user_book_argparse = argparse.ArgumentParser(description='$get-user-book')
get_user_book_argparse.add_argument('--userId', type=str, required=True)