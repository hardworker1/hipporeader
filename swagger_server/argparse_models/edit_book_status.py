import argparse

edit_book_status_argparse = argparse.ArgumentParser(description='$edit-book-status')
edit_book_status_argparse.add_argument('--bookId', type=str, required=True)
edit_book_status_argparse.add_argument('--status', type=int, default=1)