import argparse

cache_content_argparse = argparse.ArgumentParser(description='$cache-content')
cache_content_argparse.add_argument("--userId", type=str, required=True)
cache_content_argparse.add_argument("--bookId", type=str, required=True)
cache_content_argparse.add_argument("--chapterTitle", type=str, required=True)
cache_content_argparse.add_argument("--content", type=str, required=True)
cache_content_argparse.add_argument("--type", type=int, default=1)
cache_content_argparse.add_argument("--contentWordCount", type=int, default=0)
cache_content_argparse.add_argument("--authorSpeak", type=str, default='')
cache_content_argparse.add_argument("--titleIndex", type=int, default=1)