import argparse

set_user_read_book_info_argparse = argparse.ArgumentParser(description='$get-user-read-book-info')
set_user_read_book_info_argparse.add_argument('--userId', type=str, required=True)
set_user_read_book_info_argparse.add_argument('--bookId', type=str, required=True)
set_user_read_book_info_argparse.add_argument('--index', type=int, default=0, required=True)