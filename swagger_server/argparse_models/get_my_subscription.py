import argparse

get_my_subscription_argparse = argparse.ArgumentParser(description='$get-my-subscription')
get_my_subscription_argparse.add_argument('--userId', type=str, required=True)
get_my_subscription_argparse.add_argument('--pageFrom', type=int, default=0)
get_my_subscription_argparse.add_argument('--pageSize', type=int, default=12)