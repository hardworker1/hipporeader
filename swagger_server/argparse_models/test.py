import argparse

test_argparse = argparse.ArgumentParser(description="$test")
test_argparse.add_argument("--numOne", type=int, default=1)
test_argparse.add_argument("--numTwo", type=int, default=2)