import argparse

user_read_book_info_argparse = argparse.ArgumentParser(description='$user-read-book-info')
user_read_book_info_argparse.add_argument('--userId', type=str, required=True)
user_read_book_info_argparse.add_argument('--bookId', type=str, required=True)