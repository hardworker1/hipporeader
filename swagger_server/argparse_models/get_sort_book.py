import argparse

get_sort_book_argparse = argparse.ArgumentParser(description="$get-sort-book")
get_sort_book_argparse.add_argument("--userId", required=True, type=str)
get_sort_book_argparse.add_argument("--pageFrom", required=True, type=int)
get_sort_book_argparse.add_argument("--pageSize", required=True, type=int)
get_sort_book_argparse.add_argument("--type", required=True, type=str)