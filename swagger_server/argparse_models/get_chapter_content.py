import argparse

get_chapter_content_argparse = argparse.ArgumentParser(description='$get-chapter-content')
get_chapter_content_argparse.add_argument("--bookId", type=str, required=True)
get_chapter_content_argparse.add_argument("--itemId", type=str, required=True)