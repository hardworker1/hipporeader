import argparse

get_collection_book_argparse = argparse.ArgumentParser(description='$get-collection-book')
get_collection_book_argparse.add_argument('--userId', type=str, required=True)
get_collection_book_argparse.add_argument('--pageFrom', type=int, default=0)
get_collection_book_argparse.add_argument('--pageSize', type=int, default=5)