import argparse

get_author_info_argparse = argparse.ArgumentParser(description='$get-author-info')
get_author_info_argparse.add_argument('--authorId', type=str, required=True)
get_author_info_argparse.add_argument('--userId', type=str, required=True)