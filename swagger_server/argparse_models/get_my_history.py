import argparse

get_my_history_argparse = argparse.ArgumentParser(description='$get-my-history')
get_my_history_argparse.add_argument('--userId', type=str, required=True)
get_my_history_argparse.add_argument('--pageFrom', type=int, default=0)
get_my_history_argparse.add_argument('--pageSize', type=int, default=6)