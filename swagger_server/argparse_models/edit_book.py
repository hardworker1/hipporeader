import argparse

edit_book_argparse = argparse.ArgumentParser(description='$create-book')
edit_book_argparse.add_argument('--bookName', type=str, required=True)
edit_book_argparse.add_argument('--object', type=int, default=1)
edit_book_argparse.add_argument('--tags', type=str, default='')
edit_book_argparse.add_argument('--description', type=str, required=True)
edit_book_argparse.add_argument('--bookImg', type=str, required=True)
edit_book_argparse.add_argument('--bookId', type=str, required=True)