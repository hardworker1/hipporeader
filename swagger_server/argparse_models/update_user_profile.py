import argparse
import json

update_user_profile_argparse = argparse.ArgumentParser(description='$update-user-profile')
update_user_profile_argparse.add_argument('--userId', type=str)
update_user_profile_argparse.add_argument('--userProfile', type=json.loads, default={})