import argparse

create_book_argparse = argparse.ArgumentParser(description='$create-book')
create_book_argparse.add_argument('--bookName', type=str, required=True)
create_book_argparse.add_argument('--object', type=int, default=1)
create_book_argparse.add_argument('--tags', type=str, default='')
create_book_argparse.add_argument('--description', type=str, required=True)
create_book_argparse.add_argument('--bookImg', type=str, required=True)
create_book_argparse.add_argument('--userId', type=str, required=True)