import argparse

get_chapter_argparse = argparse.ArgumentParser(description='$get-chapter')
get_chapter_argparse.add_argument("--bookId", type=str, required=True)
get_chapter_argparse.add_argument("--pageFrom", type=int, default=0)
get_chapter_argparse.add_argument("--pageSize", type=int, default=10)