import argparse

subscription_author_argparse = argparse.ArgumentParser(description='$subscription-author')
subscription_author_argparse.add_argument("--userId", type=str, required=True)
subscription_author_argparse.add_argument("--authorId", type=str, required=True)
subscription_author_argparse.add_argument("--subscription", type=int, default=1)