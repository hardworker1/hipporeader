import argparse

subscription_status_argparse = argparse.ArgumentParser(description='$subscription-status')
subscription_status_argparse.add_argument('--userId', type=str, required=True)
subscription_status_argparse.add_argument('--authorId', type=str, required=True)