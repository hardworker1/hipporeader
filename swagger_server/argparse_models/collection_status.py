import argparse
collection_status_argparse = argparse.ArgumentParser(description='$subscription-status')
collection_status_argparse.add_argument('--userId', type=str, required=True)
collection_status_argparse.add_argument('--bookId', type=str, required=True)