import argparse

search_argparse = argparse.ArgumentParser(description='$search')
search_argparse.add_argument("--content", type=str, required=True)
search_argparse.add_argument("--pageSize", type=int, default=10)
search_argparse.add_argument("--pageFrom", type=int, default=0)