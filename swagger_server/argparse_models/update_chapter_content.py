import argparse

update_chapter_content_argparse = argparse.ArgumentParser(description='$update-chapter-content')
update_chapter_content_argparse.add_argument("--bookId", type=str, required=True)
update_chapter_content_argparse.add_argument("--chapterTitle", type=str, required=True)
update_chapter_content_argparse.add_argument("--content", type=str, required=True)
update_chapter_content_argparse.add_argument("--type", type=int, default=1)
update_chapter_content_argparse.add_argument("--contentWordCount", type=int, default=0)
update_chapter_content_argparse.add_argument("--authorSpeak", type=str, default='')
update_chapter_content_argparse.add_argument("--titleIndex", type=int, default=1)
update_chapter_content_argparse.add_argument("--itemId", type=str, required=True)