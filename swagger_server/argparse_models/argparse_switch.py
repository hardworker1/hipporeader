from swagger_server.argparse_models.test import test_argparse
from swagger_server.argparse_models.get_book_info import get_book_info_argparse
from swagger_server.argparse_models.get_book_detail import get_book_detail_argparse
from swagger_server.argparse_models.get_user_profile import get_user_profile_argparse
from swagger_server.argparse_models.get_book_content import get_book_content_argparse
from swagger_server.argparse_models.update_user_profile import update_user_profile_argparse
from swagger_server.argparse_models.user_read_book_info import user_read_book_info_argparse
from swagger_server.argparse_models.set_user_read_book_info import set_user_read_book_info_argparse
from swagger_server.argparse_models.subscription_author import subscription_author_argparse
from swagger_server.argparse_models.subscription_status import subscription_status_argparse
from swagger_server.argparse_models.modify_bookcase import modify_bookcase_argparse
from swagger_server.argparse_models.collection_status import collection_status_argparse
from swagger_server.argparse_models.get_bookstore_data import get_bookstore_data_argparse
from swagger_server.argparse_models.get_collection_book import get_collection_book_argparse
from swagger_server.argparse_models.delete_collection_book import delete_collection_book_argparse
from swagger_server.argparse_models.get_author_info import get_author_info_argparse
from swagger_server.argparse_models.get_my_history import get_my_history_argparse
from swagger_server.argparse_models.get_my_subscription import get_my_subscription_argparse
from swagger_server.argparse_models.create_book import create_book_argparse
from swagger_server.argparse_models.get_user_book import get_user_book_argparse
from swagger_server.argparse_models.edit_book import edit_book_argparse
from swagger_server.argparse_models.edit_book_status import edit_book_status_argparse
from swagger_server.argparse_models.get_chapter_argparse import get_chapter_argparse
from swagger_server.argparse_models.cache_content import cache_content_argparse
from swagger_server.argparse_models.delete_chapter import delete_chapter_argparse
from swagger_server.argparse_models.get_chapter_content import get_chapter_content_argparse
from swagger_server.argparse_models.update_chapter_content import update_chapter_content_argparse
from swagger_server.argparse_models.get_sort_book import get_sort_book_argparse
from swagger_server.argparse_models.search import search_argparse

def argparse_switch(command_type):
    switch = {
        "$test": test_argparse,
        "$get-book-info": get_book_info_argparse,
        "$get-book-detail": get_book_detail_argparse,
        "$get-user-profile": get_user_profile_argparse,
        "$get-book-content": get_book_content_argparse,
        "$update-user-profile": update_user_profile_argparse,
        "$user-read-book-info": user_read_book_info_argparse,
        "$set-user-read-book-info": set_user_read_book_info_argparse,
        "$subscription-author": subscription_author_argparse,
        "$subscription-status": subscription_status_argparse,
        "$modify-bookcase": modify_bookcase_argparse,
        "$collection-status": collection_status_argparse,
        "$get-bookstore-data": get_bookstore_data_argparse,
        "$get-collection-book": get_collection_book_argparse,
        "$delete-collection-book": delete_collection_book_argparse,
        "$get-author-info": get_author_info_argparse,
        "$get-my-history": get_my_history_argparse,
        "$get-my-subscription": get_my_subscription_argparse,
        "$create-book": create_book_argparse,
        "$get-user-book": get_user_book_argparse,
        "$edit-book": edit_book_argparse,
        "$edit-book-status": edit_book_status_argparse,
        "$get-chapter": get_chapter_argparse,
        "$cache-content": cache_content_argparse,
        "$delete-chapter": delete_chapter_argparse,
        "$get-chapter-content": get_chapter_content_argparse,
        "$update-chapter-content": update_chapter_content_argparse,
        "$get-sort-book": get_sort_book_argparse,
        "$search": search_argparse,
    }

    return switch[command_type]
