import argparse

delete_collection_book_argparse = argparse.ArgumentParser(description='$delete-collection-book')
delete_collection_book_argparse.add_argument('--userId', type=str, required=True)
delete_collection_book_argparse.add_argument('--bookId', type=str, required=True)