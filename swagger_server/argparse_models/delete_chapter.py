import argparse

delete_chapter_argparse = argparse.ArgumentParser(description="$delete-chapter")
delete_chapter_argparse.add_argument("--bookId", type=str, required=True)
delete_chapter_argparse.add_argument("--itemId", type=str, required=True)