import argparse

get_bookstore_data_argparse = argparse.ArgumentParser(description='$get-bookstore-data')
get_bookstore_data_argparse.add_argument('--userId', type=str, required=True)
get_bookstore_data_argparse.add_argument('--pageFrom', type=int, default=0)
get_bookstore_data_argparse.add_argument('--pageSize', type=int, default=15)
get_bookstore_data_argparse.add_argument('--type', type=int, default=1)
