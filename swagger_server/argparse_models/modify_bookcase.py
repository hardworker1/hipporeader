import argparse

modify_bookcase_argparse = argparse.ArgumentParser(description='$modify-bookcase')
modify_bookcase_argparse.add_argument("--userId", type=str, required=True)
modify_bookcase_argparse.add_argument("--bookId", type=str, required=True)
modify_bookcase_argparse.add_argument("--type", type=int, default=1, required=True)