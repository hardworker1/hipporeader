from swagger_server.api_providers.data_provider import DataProvider
from swagger_server.utils.oms_client import OmsClient
from swagger_server.constants import *

class SubscriptionStatus(DataProvider):
    def __init__(self, param_dict):
        DataProvider.__init__(self, param_dict)
        self.user_id = param_dict.get("userId")
        self.author_id = param_dict.get('authorId')

    def build_response(self):
        oms_client = OmsClient(OMS_RETRIEVE_URL, OMS_NAMESPACE_USER_PROFILE)
        try:
            data_key = "regex::{}.*".format(self.user_id)
            user_profile = oms_client.get_first_value(data_key)
        except:
            return {"error": "get user profile failed, userId={}".format(self.user_id)}
        if not user_profile:
            return {'error': '获取用户信息失败，请退出后重新登入'}
        data_value = user_profile.get('dataValue')
        subscription_author = data_value.get('subscription')
        if not subscription_author or self.author_id not in subscription_author:
            return {'data': {'status': 0}}
        return {'data': {'status': 1}} 