from swagger_server.api_providers.data_provider import DataProvider
from swagger_server.utils.oms_client import OmsClient
from swagger_server.constants import *

class BookInfoModel:
    def __init__(self):
        self.bookName = ''
        self.bookId = ''
        self.bookImg = ''
        self.bookAbstract = ''
        self.lastChapterTitle = ''
        self.lastPublishTime = ''
        self.bookStatus = ''
        self.collectionStatus = 0
        self.tags = []
        self.wordCount = 0
        self.authorName = ''

class Search(DataProvider):
    def __init__(self, params):
        DataProvider.__init__(self, params)
        self.content = params.get('content')
        self.page_from = params.get("pageFrom")
        self.page_size = params.get("pageSize")
        self.total_size = 0

    def build_response(self):
        oms_client = OmsClient(OMS_RETRIEVE_URL, OMS_NAMESPACE_BOOK_PROFILE)
        try:
            result = oms_client.get_all_values('regex::.*')
        except:
            return {'error': '获取书籍信息失败'}
        items = result.get('items')
        book_similarity = []
        book_info_dict = {}
        for item in items:
            book_name = item.get('dataValue').get('bookName')
            author_name = item.get('dataValue').get('authorName')
            book_id = item.get('dataValue').get('bookId')
            book_name_similarity = self.get_similarity(book_name, self.content)
            author_name_similarity = self.get_similarity(author_name, self.content)
            similarity_value = max(book_name_similarity, author_name_similarity)
            if similarity_value > 0:
                book_similarity.append({'bookId': book_id, 'value': similarity_value})
                book_info_dict[book_id] = item.get('dataValue')
        book_similarity = sorted(book_similarity, key=lambda x: x['value'], reverse=True)
        self.total_size = len(book_similarity)
        if self.page_from + self.page_size < self.total_size:
            need_book = book_similarity[self.page_from: self.page_from + self.page_size]
        else:
            need_book = book_similarity[self.page_from:]
        bookInfoItems = []
        content_list = list(self.content)
        for item in need_book:
            item = book_info_dict[item['bookId']]
            book_info = BookInfoModel()
            book_info.bookId = item.get('bookId')
            book_info.bookImg = item.get('bookImg')
            book_name = ''
            for i in range(len(item.get('bookName'))):
                if item.get('bookName')[i] in content_list:
                    book_name += '<span style="color:red;">' + item.get('bookName')[i] + "</span>"
                else:
                    book_name += item.get('bookName')[i]
            book_info.bookName = book_name
            author_name = '作者：'
            for i in range(len(item.get('authorName'))):
                if item.get('authorName')[i] in content_list:
                    author_name += '<span style="color:red;">' + item.get('authorName')[i] + "</span>"
                else:
                    author_name += item.get('authorName')[i]
            book_info.authorName = author_name
            book_info.bookStatus = BOOK_STATUS[item.get('bookStatus')]
            book_info.lastChapterTitle = item.get('lastChapterTitle')
            book_info.lastChapterIndex = len(item.get('directoryInfo')) - 1
            book_info.lastPublishTime = item.get('lastPublishTime')
            book_info.tags = []
            for tag in item.get('bookTags'):
                book_info.tags.append(BOOK_TAGS[tag])
            book_info.wordCount = item.get('wordCount')
            book_info.bookAbstract = item.get('bookAbstract')
            bookInfoItems.append(book_info.__dict__)
        return {
            'data': {
                'totalSize': self.total_size,
                'bookInfoItems': bookInfoItems
            }
        }

    def get_similarity(self, str1, str2):
        len1 = len(str1)
        len2 = len(str2)
        distance_matrix = [[abs(i-j) for i in range(len2+1)] for j in range(len1+1)]
        for i in range(len1):
            for j in range(len2):
                if str1[i] == str2[j]:
                    distance = 0
                else:
                    distance = 1
                distance_matrix[i+1][j+1] = min(distance_matrix[i][j] + distance, distance_matrix[i][j+1]+1, distance_matrix[i+1][j]+1)
        return 1 - distance_matrix[len1][len2] / max(len1, len2)

if __name__ == '__main__':
    params = {'content': '天生', 'pageFrom': 0, 'pageSize': 10}
    print(Search(params).build_response())