from swagger_server.api_providers.data_provider import DataProvider
from swagger_server.utils.oms_client import OmsClient
from swagger_server.constants import *


class DeleteChapter(DataProvider):
    def __init__(self, params):
        DataProvider.__init__(self, params)
        self.book_id = params.get('bookId')
        self.item_id = params.get("itemId")

    def build_response(self):
        oms_client = OmsClient(OMS_RETRIEVE_URL, OMS_NAMESPACE_BOOK_PROFILE)
        try:
            result = oms_client.get_first_value("regex::.*#{}.*".format(self.book_id))
        except:
            return {'error': '删除失败'}
        data_value = result.get("dataValue")
        directory_info = data_value.get("directoryInfo")
        for index in range(len(directory_info)):
            if directory_info[index].get("itemId") == self.item_id:
                directory_info.pop(index)
                break
        data_key = result.get('dataKey')
        update_oms_client = OmsClient(OMS_UPDATE_URL, OMS_NAMESPACE_BOOK_PROFILE)
        try:
            update_oms_client.update(data_key, data_value)
        except:
            return {'error': '删除失败'}
        delete_oms_client = OmsClient(OMS_DELETE_URL, OMS_NAMESPACE_BOOK_CONTENT)
        try:
            delete_oms_client.delete(self.item_id)
        except:
            return {'error': '删除失败'}
        return {'data': 'success'}