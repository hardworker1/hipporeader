from swagger_server.utils.oms_client import OmsClient
from swagger_server.api_providers.data_provider import DataProvider
from swagger_server.constants import *


class GetChapter(DataProvider):
    def __init__(self, param_dict):
        self.book_id = param_dict.get("bookId")
        self.page_from = param_dict.get("pageFrom")
        self.page_size = param_dict.get("pageSize")
        self.total_size = 0
        self.book_status = 2
        DataProvider.__init__(self, param_dict)

    def build_response(self):
        oms_client = OmsClient(OMS_RETRIEVE_URL, OMS_NAMESPACE_BOOK_PROFILE)
        try:
            data_key = "regex::^.*#{}.*$".format(self.book_id)
            data = oms_client.get_first_value(data_key)
        except:
            return {"error": "get book information error, bookId={}".format(self.book_id)}
        if data and data.get("dataValue") and data.get('dataValue').get("directoryInfo"):
            data_value = data.get("dataValue")
            self.book_status = data_value.get('bookStatus')
            directory_info = data_value.get('directoryInfo')
            self.total_size = len(directory_info)
            item_id_list = []
            for index in range(self.page_size):
                new_index = index + self.page_from
                if new_index >= self.total_size or new_index > self.page_from + self.page_size:
                    break
                if 'type' in directory_info[new_index]:
                    directory_info[new_index]['status'] = CHAPTER_STATUS[directory_info[new_index]['type']]
                else:
                    directory_info[new_index]['status'] = CHAPTER_STATUS[1]
                item_id_list.append(directory_info[new_index]['itemId'])
            oms_client = OmsClient(OMS_RETRIEVE_URL, OMS_NAMESPACE_BOOK_CONTENT)
            data_key = 'regex::({})'.format('|'.join(item_id_list))
            try:
                result = oms_client.get_all_values(data_key)
            except:
                return {'error': '获取目录信息失败'}
            word_count = {}
            for item in result.get('items'):
                data_value = item.get('dataValue')
                word_count[data_value['itemId']] = {
                    'contentWordCount': data_value['contentWordCount'],
                    'contentUpdateTime': data_value['contentUpdateTime']
                }
            chapter_info_items = []
            for index in range(self.page_size):
                new_index = index + self.page_from
                if new_index >= self.total_size or new_index > self.page_from + self.page_size:
                    break
                chapter_info = directory_info[new_index]
                chapter_info['contentWordCount'] = word_count[directory_info[new_index]['itemId']]["contentWordCount"]
                chapter_info['contentUpdateTime'] = word_count[directory_info[new_index]['itemId']]["contentUpdateTime"]
                chapter_info_items.append(chapter_info)
            return {
                'data': {
                    'totalSize': self.total_size,
                    'chapterInfoItems': chapter_info_items
                }
            }
        return {'data': {
            'totalSize': 0,
            'chapterInfoItems': []
        }}


if __name__ == '__main__':
    params = {
        'bookId': '6846366104005118983',
        'pageFrom': 0,
        'pageSize': 10
    }
    print(GetChapter(params).build_response())
