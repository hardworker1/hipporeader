from swagger_server.api_providers.data_provider import DataProvider
from swagger_server.utils.oms_client import OmsClient
from swagger_server.constants import *

class ResponseData:
    def __init__(self):
        self.bookName = None
        self.authorName = None
        self.bookStatus = None
        self.wordCount = None
        self.readCount = None
        self.bookBrief = None
        self.lastPublishTime = None
        self.bookId = None
        self.bookImg = None

class GetSortBook(DataProvider):
    def __init__(self, params):
        DataProvider.__init__(self, params)
        self.page_from = params.get("pageFrom")
        self.page_size = params.get("pageSize")
        self.type = params.get("type")

    def build_response(self):
        oms_client = OmsClient(OMS_RETRIEVE_URL, OMS_NAMESPACE_BOOK_PROFILE)
        try:
            result = oms_client.get_all_values('regex::.*#{}(#|$).*'.format(self.type))
        except:
            return {'error': '获取书籍信息失败，请重新操作'}
        if not result:
            return {'data': {
                'totalSize': 0,
                'bookInfo': []
            }}
        total_size = result.get('totalSize')
        items = result.get('items')
        sort_by = 1
        sort_items = sorted(items, key=lambda x: x["dataValue"]['readerCount'], reverse=True)
        if self.page_from + self.page_size < total_size:
            need_items = sort_items[self.page_from: self.page_from+self.page_size]
        else:
            need_items = sort_items[self.page_from:]
        response_data = []
        for item in need_items:
            data_value = item.get('dataValue')
            response = ResponseData()
            response.bookId = data_value.get('bookId')
            response.bookImg = data_value.get('bookImg')
            response.authorName = data_value.get('authorName')
            response.bookBrief = data_value.get('bookAbstract')
            response.bookName = data_value.get('bookName')
            response.bookStatus = BOOK_STATUS[data_value.get('bookStatus')]
            response.readCount = data_value.get('readerCount')
            response.wordCount = data_value.get('wordCount')
            response.lastPublishTime = data_value.get('lastPublishTime')
            response_data.append(response.__dict__)
        return {'data': {
            'bookInfo': response_data,
            'totalSize': total_size
        }}