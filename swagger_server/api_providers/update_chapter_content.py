from swagger_server.api_providers.data_provider import DataProvider
from swagger_server.utils.oms_client import OmsClient
from swagger_server.constants import *
import datetime

class UpdateChapterContent(DataProvider):
    def __init__(self, params):
        DataProvider.__init__(self, params)
        self.book_id = params.get("bookId")
        self.chapter_title = params.get("chapterTitle")
        self.content = params.get("content")
        self.type = params.get("type")
        self.content_word_content = params.get("contentWordCount")
        self.author_speak = params.get('authorSpeak')
        self.title_index = params.get('titleIndex')
        self.item_id = params.get('itemId')

    def build_response(self):
        oms_client = OmsClient(OMS_RETRIEVE_URL, OMS_NAMESPACE_BOOK_PROFILE)
        data_key = "regex::.*#{}.*".format(self.book_id)
        try:
            result = oms_client.get_first_value(data_key)
        except:
            return {'error': '从数据库中获取数据信息失败'}
        data_value = result.get("dataValue")
        directory_info = data_value.get("directoryInfo")
        new_data = {
            "itemId": self.item_id,
            "type": self.type,
            "title": '第' + str(self.title_index) + '章 ' + self.chapter_title ,
            "titleIndex": self.title_index,
            "chapterTitle": self.chapter_title
        }
        data_key = result.get('dataKey')
        old_type = 2
        is_last_chapter = False
        for index in range(len(directory_info)):
            if directory_info[index]["itemId"] == self.item_id:
                if 'type' in directory_info[index]:
                    old_type = directory_info[index]['type']
                if index == len(directory_info) - 1:
                    is_last_chapter = True
                directory_info[index] = new_data
        data_value['directoryInfo'] = directory_info
        if old_type == 2 and self.type == 1:
            data_value['lastPublishTime'] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            data_value['wordCount'] = data_value['wordCount'] + self.content_word_content
        elif old_type == 1 and self.type == 1:
            content_oms_client = OmsClient(OMS_RETRIEVE_URL, OMS_NAMESPACE_BOOK_CONTENT)
            try:
                result = content_oms_client.get_first_value(self.item_id)
            except:
                return {'error': '保存新章节失败'}
            word_count = result.get('dataValue').get('contentWordCount')
            data_value['wordCount'] = data_value['wordCount'] - word_count + self.content_word_content
            if is_last_chapter:
                data_value['lastChapterTitle'] = '第' + str(self.title_index) + '章 ' + self.chapter_title
        elif old_type == 1 and self.type == 2:
            word_count = result.get('dataValue').get('contentWordCount')
            data_value['wordCount'] = data_value['wordCount'] - word_count
            if is_last_chapter:
                data_value['lastChapterTitle'] = directory_info[len(directory_info)-2]['title']
        oms_client = OmsClient(OMS_UPDATE_URL, OMS_NAMESPACE_BOOK_PROFILE)
        try:
            oms_client.update(data_key, data_value)
        except:
            return {'error': '保存新章节失败'}
        data_value = {
            "content": self.content,
            "contentUpdateTime": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            "contentWordCount": self.content_word_content,
            "authorSpeak": self.author_speak,
            "itemId": self.item_id
        }
        oms_client = OmsClient(OMS_UPDATE_URL, OMS_NAMESPACE_BOOK_CONTENT)
        try:
            oms_client.update(self.item_id, data_value)
        except:
            return {'error': '保存新章节失败'}
        return {'data': 'success'}