from swagger_server.utils.oms_client import OmsClient
from swagger_server.api_providers.data_provider import DataProvider
from swagger_server.constants import *

class BookInfoModel:
    def __init__(self):
        self.bookId = None
        self.bookName = None
        self.lastChapterTitle = None
        self.lastChapterIndex = 0
        self.lastPublishTime = None
        self.bookImg = None
        self.directoryTotal = 0
        self.wordCount = 0
        self.status = 0

class GetUserBook(DataProvider):
    def __init__(self, param_dict):
        self.user_id = param_dict.get("userId")
        DataProvider.__init__(self, param_dict)

    def build_response(self):
        oms_client = OmsClient(OMS_RETRIEVE_URL, OMS_NAMESPACE_BOOK_PROFILE)
        try:
            data_key = "regex::^{}.*$".format(self.user_id)
            data = oms_client.get_all_values(data_key)
        except:
            return {"error": "get book information error, userId={}".format(self.user_id)}
        items = data.get('items')
        total_size = data.get('totalSize')
        book_info_items = []
        for item in items:
            data_value = item.get("dataValue")
            book_info = BookInfoModel()
            book_info.bookId = data_value.get('bookId')
            book_info.bookName = data_value.get('bookName')
            book_status = data_value.get("bookStatus")
            book_info.status = BOOK_STATUS[book_status]
            book_info.bookImg = data_value.get('bookImg')
            directory_info = data_value.get('directoryInfo')
            for index in range(len(directory_info)):
                info = directory_info[index]
                if 'type' in info and info['type'] == 1:
                    book_info.lastChapterIndex = index
                    book_info.directoryTotal += 1
            book_info.lastPublishTime = data_value.get('lastPublishTime')
            book_info.lastChapterTitle = data_value.get('lastChapterTitle')
            book_info.wordCount = data_value.get('wordCount')
            book_info_items.append(book_info.__dict__)
        return {
            'data': {
                'totalSize': total_size,
                'bookInfoItems': book_info_items
            }
        }
