from swagger_server.api_providers.data_provider import DataProvider
from swagger_server.utils.oms_client import OmsClient
from swagger_server.constants import *

class GetMySubscription(DataProvider):
    def __init__(self, params):
        DataProvider.__init__(self, params)
        self.user_id = params.get("userId")
        self.page_from = params.get('pageFrom')
        self.page_size = params.get('pageSize')
        self.total_size = 0

    def build_response(self):
        status, subscription_author = self.get_subscription_author()
        if not status:
            return subscription_author
        status, author_info_items = self.get_author_info(subscription_author)
        if not status:
            return author_info_items
        return {
            'data': {
                'totalSize': self.total_size,
                'authorInfoItems': author_info_items
            }
        }

    def get_subscription_author(self):
        oms_client = OmsClient(OMS_RETRIEVE_URL, OMS_NAMESPACE_USER_PROFILE)
        try:
            result = oms_client.get_first_value('regex::^{}#.*$'.format(self.user_id))
        except:
            return False, {'error': '获取关注作者的信息失败'}
        if not result:
            return True, []
        subscription_author = result.get('dataValue').get('subscription')
        self.total_size = len(subscription_author)
        if self.page_size + self.page_from < len(subscription_author):
            subscription_author = subscription_author[self.page_from: self.page_from + self.page_size]
        else:
            subscription_author = subscription_author[self.page_from:]
        return True, subscription_author

    def get_author_info(self, subscription_author):
        author_id_str = '|'.join(subscription_author)
        data_key = 'regex::^({}).*'.format(author_id_str)
        oms_client = OmsClient(OMS_RETRIEVE_URL, OMS_NAMESPACE_USER_PROFILE)
        try:
            result = oms_client.get_all_values(data_key)
        except:
            return False, {'error': '获取关注作者的信息错误'}
        author_items = result.get('items')
        author_info_items_dict = {}
        for item in author_items:
            author_info = {}
            data_value = item.get('dataValue')
            author_info['authorName'] = data_value.get('userName')
            author_info['authorId'] = data_value.get('userId')
            author_info['authorDesc'] = data_value.get('description')
            author_info['authorImg'] = data_value.get('image')
            author_info_items_dict[data_value.get('userId')] = author_info
        author_info_items = []
        for author_id in subscription_author:
            author_info_items.append(author_info_items_dict[author_id])
        return True, author_info_items

if __name__ == '__main__':
    params = {
        'userId': 'a87caac1-f14d-4372-9b3e-4bb837902f4d',
        'pageFrom': 12,
        'pageSize': 12
    }
    print(
        GetMySubscription(params).build_response()
    )

