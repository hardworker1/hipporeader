from swagger_server.api_providers.data_provider import DataProvider
from swagger_server.utils.oms_client import OmsClient
from swagger_server.constants import *

class SubscriptionAuthor(DataProvider):
    def __init__(self, param_dict):
        DataProvider.__init__(self, param_dict)
        self.userId = param_dict.get('userId')
        self.authorId = param_dict.get('authorId')
        self.subscription = param_dict.get("subscription")

    def build_response(self):
        retrieve_oms_client = OmsClient(OMS_RETRIEVE_URL, OMS_NAMESPACE_USER_PROFILE)
        try:
            result = retrieve_oms_client.get_first_value('regex::^{}#.*'.format(self.userId))
        except:
            if self.subscription == 1:
                return {'error': '订阅失败, 请重新操作'}
            else:
                return {'error': '取消订阅失败，请重新操作'}
        if not result:
            return {'error': '获取用户信息失败，请退出后重新登入'}
        data_value = result.get('dataValue')
        if 'subscription' not in data_value:
            data_value['subscription'] = []
        if self.subscription == 1:
            if self.authorId not in data_value['subscription']:
                subscription_author = [self.authorId]
                subscription_author.extend(data_value['subscription'])
                data_value['subscription'] = subscription_author
        else:
            for i in range(len(data_value.get('subscription'))):
                if data_value['subscription'][i] == self.authorId:
                    data_value['subscription'].pop(i)
                    break
        data_key = result.get('dataKey')
        update_oms_client = OmsClient(OMS_UPDATE_URL, OMS_NAMESPACE_USER_PROFILE)
        try:
            update_oms_client.update(data_key, data_value)
        except:
            if self.subscription == 1:
                return {'error': '订阅失败, 请重新操作'}
            else:
                return {'error': '取消订阅失败，请重新操作'}
        return {'data': 'success'}