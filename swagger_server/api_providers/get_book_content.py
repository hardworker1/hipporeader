from swagger_server.utils.oms_client import OmsClient
from swagger_server.api_providers.data_provider import DataProvider
from swagger_server.constants import *

class GetBookContent(DataProvider):
    def __init__(self, param):
        DataProvider.__init__(self, param)
        self.content_id = param.get("contentId")

    def build_response(self):
        oms_client = OmsClient(OMS_RETRIEVE_URL, OMS_NAMESPACE_BOOK_CONTENT)
        try:
            content_profile = oms_client.get_first_value(self.content_id)
        except:
            return {"error": "get book content failed, contentId={}".format(self.content_id)}
        if content_profile and content_profile.get("dataValue"):
            if 'authorSpeak' in content_profile.get("dataValue") and content_profile["dataValue"]["authorSpeak"]:
                content_profile['dataValue']["content"] += "<p>(" + content_profile["dataValue"]["authorSpeak"] + ")</p>"
            return {"data": content_profile.get("dataValue")}
        return {"data": None}


if __name__ == '__main__':
    param = {"contentId": "6846375878276940299"}
    print( GetBookContent(param).build_response())