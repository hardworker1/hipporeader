from swagger_server.api_providers.data_provider import DataProvider
from swagger_server.utils.oms_client import OmsClient
from swagger_server.constants import *

class UpdateUserProfile(DataProvider):
    def __init__(self, params):
        DataProvider.__init__(self, params)
        self.params = params
        self.user_id = params.get('userId')
        self.user_profile = params.get('userProfile')

    def build_response(self):
        oms_client = OmsClient(OMS_RETRIEVE_URL, OMS_NAMESPACE_USER_PROFILE)
        try:
            data_key = "regex::^{}#.*".format(self.user_id)
            result = oms_client.get_first_value(data_key)
            if not result:
                return {'error': '获取用户信息失败'}
            data_value = result.get("dataValue")
            data_value['description'] = self.user_profile.get('description')
            data_value['sex'] = self.user_profile.get('sex')
            data_value['birthday'] = self.user_profile.get('birthday')
            data_value['address'] = self.user_profile.get('address')
            data_value['image'] = self.user_profile.get('image')
            if (self.user_profile.get('userName') != data_value.get('userName')):
                data_value['userName'] = self.user_profile.get('userName')
                delete_oms_client = OmsClient(OMS_DELETE_URL, OMS_NAMESPACE_USER_PROFILE)
                data_key = result.get('dataKey')
                delete_oms_client.delete(data_key)
                create_oms_client = OmsClient(OMS_CREATE_URL, OMS_NAMESPACE_USER_PROFILE)
                data_key = self.user_id + "#" + data_value.get('phone') + "#" + data_value.get('email') + "#" + self.user_profile.get('userName')
                create_oms_client.create(data_key, data_value)
            else:
                update_oms_client = OmsClient(OMS_UPDATE_URL, OMS_NAMESPACE_USER_PROFILE)
                update_oms_client.update(result.get('dataKey'), data_value)
            return {'data': 'success'}
        except:
            return {'error': '保存用户信息失败，请重新操作'}

if __name__ == '__main__':
    data_key = 'a87caac1-f14d-4372-9b3e-4bb837902f4d#13970723540#2657841387@qq.com#刘水萍12'
    data_value = {
          "address": {
            "area": "吴中区",
            "city": "苏州市",
            "province": "江苏省"
          },
          "birthday": "2020-01-30",
          "bookId": [],
          "description": "我叫刘水萍，来自江西赣州",
          "email": "2657841387@qq.com",
          "image": "blob:http://localhost:8080/4ec7c41f-53e4-499e-80dc-2b67f67a79aa",
          "password": "gAAAAABgbQvDCQIhYTfBFPi8UnqxhaEGFeOXeCv7e110nbHw3VVyMdCLzf6HetFkf6NrNwmARn7Efba37O8yPMaRdaM8Mz2nJA==",
          "phone": "13970723540",
          "sex": 1,
          "subscription": [
            "6d9d0823-bfaf-440d-813e-6ce9963328e8",
            "044ea82d-1905-468d-8916-777af4fe7419",
            "1fbbdb5d-bb76-4696-925f-594821e703d6",
            "f8b5f99d-9ab1-46db-ae82-19816226a8bb",
            "b8745e5c-c75f-41c3-a240-e833474627c6",
            "ce25ed55-58b7-4ad0-b31c-a7e164516351"
          ],
          "userId": "a87caac1-f14d-4372-9b3e-4bb837902f4d",
          "userName": "刘水萍12"
        }
    update_oms_client = OmsClient(OMS_UPDATE_URL, OMS_NAMESPACE_USER_PROFILE)
    update_oms_client.update(data_key, data_value)


