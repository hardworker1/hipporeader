from swagger_server.utils.oms_client import OmsClient
from swagger_server.api_providers.data_provider import DataProvider
from swagger_server.constants import *


class GetBookDetail(DataProvider):
    def __init__(self, param_dict):
        self.book_id = param_dict.get("bookId")
        DataProvider.__init__(self, param_dict)

    def build_response(self):
        oms_client = OmsClient(OMS_RETRIEVE_URL, OMS_NAMESPACE_BOOK_PROFILE)
        try:
            data_key = "regex::^.*#{}.*".format(self.book_id)
            data = oms_client.get_first_value(data_key)
        except:
            return {"error": "get book information error, bookId={}".format(self.book_id)}
        if data and data.get("dataValue"):
            data_value = data.get("dataValue")
            book_status = data_value.get("bookStatus")
            book_status_value = BOOK_STATUS[book_status]
            data_value["bookStatus"] = book_status_value
            book_tags = data_value.get("bookTags")
            book_tags_value = []
            for tag in book_tags:
                if tag in BOOK_TAGS:
                    book_tags_value.append(BOOK_TAGS[tag])
            data_value["bookTags"] = book_tags_value
            directory_info = []
            for item in data_value.get("directoryInfo"):
                if 'type' not in item or item['type'] == 1:
                    directory_info.append(item)
            data_value["directoryInfo"] = directory_info
            return {"data": data_value}
        return {"data": None}

if __name__ == '__main__':
    ob = GetBookDetail({"bookId": "6846366104005118983"}).build_response()
    print(ob)