from swagger_server.api_providers.data_provider import DataProvider
from swagger_server.utils.oms_client import OmsClient
from swagger_server.constants import *

class UserReadBookInfo(DataProvider):
    def __init__(self, param_dict):
        DataProvider.__init__(self, param_dict)
        self.user_id = param_dict.get("userId")
        self.bookId = param_dict.get('bookId')

    def build_response(self):
        oms_client = OmsClient(OMS_RETRIEVE_URL, OMS_NAMESPACE_USER_READ_BOOK_INFO)
        try:
            result = oms_client.get_first_value(self.user_id)
        except:
            return {'data': {'index': 0}}
        if not result:
            return {'data': {'index': 0}}
        data_value = result.get('dataValue')
        view_history = [self.bookId]
        if self.bookId not in data_value.get('viewHistory'):
            view_history.extend(data_value.get('viewHistory'))
            data_value['viewHistory'] = view_history
        else:
            for index in range(len(data_value.get('viewHistory'))):
                if data_value['viewHistory'][index] == self.bookId:
                    data_value['viewHistory'].pop(index)
                    break
            view_history.extend(data_value.get('viewHistory'))
            data_value['viewHistory'] = view_history
        update_oms_client = OmsClient(OMS_UPDATE_URL, OMS_NAMESPACE_USER_READ_BOOK_INFO)
        try:
            update_oms_client.update(self.user_id, data_value)
        except:
            pass
        if self.bookId in data_value.get('bookIndex'):
            return {'data': {'index': data_value['bookIndex'][self.bookId]}}
        return {'data': {'index': 0}}