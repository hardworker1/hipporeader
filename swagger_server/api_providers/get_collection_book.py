from swagger_server.api_providers.data_provider import DataProvider
from swagger_server.utils.oms_client import OmsClient
from swagger_server.constants import *

class ResponseItemModel:
    def __init__(self):
        self.bookId = None
        self.bookName = None
        self.lastChapterTitle = None
        self.lastChapterIndex = 0
        self.authorName = None
        self.bookImg = None

class GetCollectionBook(DataProvider):
    def __init__(self, param_dict):
        DataProvider.__init__(self, param_dict)
        self.user_id = param_dict.get("userId")
        self.page_from = param_dict.get('pageFrom')
        self.page_size = param_dict.get('pageSize')

    def build_response(self):
        oms_client = OmsClient(OMS_RETRIEVE_URL, OMS_NAMESPACE_USER_BOOKCASE)
        try:
            result = oms_client.get_first_value(self.user_id)
        except:
            return {"error": "get user profile failed, userId={}".format(self.user_id)}
        if not result:
            return {'data': {
                'totalSize': 0,
                'bookInfo': []
            }}
        bookcase = result.get('dataValue').get('bookcase')
        total_size = len(bookcase)
        if self.page_from + self.page_size < len(bookcase):
            need_bookcase = bookcase[self.page_from: self.page_from+self.page_size]
        else:
            need_bookcase = bookcase[self.page_from:]
        bookcase_sort = {}
        for i in range(len(need_bookcase)):
            bookcase_sort[need_bookcase[i]] = {}
        book_id_str = '|'.join(need_bookcase)
        book_oms_client = OmsClient(OMS_RETRIEVE_URL, OMS_NAMESPACE_BOOK_PROFILE)
        data_key = 'regex::.*#({}).*'.format(book_id_str)
        try:
            result = book_oms_client.get_all_values(data_key)
        except:
            return {"error": "get user profile failed, userId={}".format(self.user_id)}
        if not result:
            return {'data': {
                'totalSize': 0,
                'bookInfo': []
            }}
        items = result.get('items')
        for item in items:
            data_value = item.get('dataValue')
            response_item = ResponseItemModel()
            response_item.bookId = data_value.get('bookId')
            response_item.bookName = data_value.get('bookName')
            response_item.bookImg  = data_value.get('bookImg')
            response_item.authorName = data_value.get('authorName')
            response_item.lastChapterTitle = data_value.get('lastChapterTitle')
            response_item.lastChapterIndex = len(data_value.get('directoryInfo')) - 1
            bookcase_sort[response_item.bookId] = response_item.__dict__
        return {
            'data': {
                'totalSize': total_size,
                'bookInfo': list(bookcase_sort.values())
            }
        }

if __name__ == '__main__':
    param = {
        'userId': 'a87caac1-f14d-4372-9b3e-4bb837902f4d',
        'pageFrom': 5,
        'pageSize': 5
    }
    print(GetCollectionBook(param).build_response()
          )