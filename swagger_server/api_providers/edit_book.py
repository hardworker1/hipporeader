from swagger_server.api_providers.data_provider import DataProvider
from swagger_server.utils.oms_client import OmsClient
from swagger_server.constants import *
import json

class EditBook(DataProvider):
    def __init__(self, params):
        DataProvider.__init__(self, params)
        self.book_name = params.get('bookName')
        self.book_img = params.get('bookImg')
        self.tags = params.get('tags')
        self.description = params.get('description')
        self.object = params.get('object')
        self.book_id = params.get('bookId')

    def build_response(self):
        oms_client = OmsClient(OMS_RETRIEVE_URL, OMS_NAMESPACE_BOOK_PROFILE)
        data_key = 'regex::。*#{}.*'.format(self.book_id)
        try:
            result = oms_client.get_first_value(data_key)
        except:
            return {'error': '更新失败，请重新操作'}
        if not result:
            return {'error': '更新失败，请重新操作'}
        data_key = result.get('dataKey')
        delete_oms_client = OmsClient(OMS_DELETE_URL, OMS_NAMESPACE_BOOK_PROFILE)
        try:
            delete_oms_client.delete(data_key)
        except:
            return {'error': '更新失败，请重新操作'}
        data_value = result.get('dataValue')
        data_value['bookName'] = self.book_name
        data_value['bookImg'] = self.book_img
        data_value['bookTags'] = json.loads(self.tags)
        data_value['bookAbstract'] = self.description
        data_value['object'] = self.object
        create_oms_client = OmsClient(OMS_CREATE_URL, OMS_NAMESPACE_BOOK_PROFILE)
        user_id = data_value.get('authorId')
        data_key = user_id + '#' + self.book_id
        for tag in self.tags:
            data_key = data_key + '#' + str(tag)
        try:
            create_oms_client.create(data_key, data_value)
        except:
            return {'error': '更新书籍失败，请重新操作'}
        return {'data': 'success'}
