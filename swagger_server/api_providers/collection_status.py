from swagger_server.api_providers.data_provider import DataProvider
from swagger_server.utils.oms_client import OmsClient
from swagger_server.constants import *

class CollectionStatus(DataProvider):
    def __init__(self, param_dict):
        DataProvider.__init__(self, param_dict)
        self.user_id = param_dict.get("userId")
        self.book_id = param_dict.get('bookId')

    def build_response(self):
        oms_client = OmsClient(OMS_RETRIEVE_URL, OMS_NAMESPACE_USER_BOOKCASE)
        try:
            result = oms_client.get_first_value(self.user_id)
        except:
            return {"error": "get user profile failed, userId={}".format(self.user_id)}
        if not result:
            return {'data': {'status': 0}}
        data_value = result.get('dataValue')
        bookcase = data_value.get('bookcase')
        if not bookcase or self.book_id not in bookcase:
            return {'data': {'status': 0}}
        return {'data': {'status': 1}}