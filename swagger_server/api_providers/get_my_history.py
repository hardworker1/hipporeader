from swagger_server.api_providers.data_provider import DataProvider
from swagger_server.utils.oms_client import OmsClient
from swagger_server.constants import *

class BookInfoModel:
    def __init__(self):
        self.bookName = ''
        self.bookId = ''
        self.bookImg = ''
        self.bookAbstract = ''
        self.lastChapterTitle = ''
        self.lastPublishTime = ''
        self.bookStatus = ''
        self.collectionStatus = 0
        self.tags = []
        self.wordCount = 0

class GetMyHistory(DataProvider):
    def __init__(self, params):
        DataProvider.__init__(self, params)
        self.user_id = params.get('userId')
        self.page_from = params.get('pageFrom')
        self.page_size = params.get('pageSize')
        self.total_size = 0

    def build_response(self):
        status, history_book = self.get_history_book()
        if not status:
            return history_book
        status, book_info_items = self.get_book_info(history_book)
        if not status:
            return book_info_items
        return {
            'data': {
                'bookInfoItems': book_info_items,
                'totalSize': self.total_size
            }
        }

    def get_book_info(self, book_id_list):
        oms_client = OmsClient(OMS_RETRIEVE_URL, OMS_NAMESPACE_BOOK_PROFILE)
        book_id_str = '|'.join(book_id_list)
        data_key = 'regex::.*#({}).*'.format(book_id_str)
        try:
            result = oms_client.get_all_values(data_key)
        except:
            return False, {'error': '获取书籍信息失败'}
        if not result:
            return False, {
                'data': {
                    'totalSize': 0,
                    'bookInfoItems': []
                }
            }
        items = result.get('items')
        book_info_items = {}
        collection_book = self.get_collection_book()
        for item in items:
            book_info = BookInfoModel()
            data_value = item.get('dataValue')
            book_info.bookId = data_value.get('bookId')
            book_info.bookImg = data_value.get('bookImg')
            book_info.bookName = data_value.get('bookName')
            book_info.bookStatus = BOOK_STATUS[data_value.get('bookStatus')]
            if book_info.bookId not in collection_book:
                book_info.collectionStatus = 0
            else:
                book_info.collectionStatus = 1
            book_info.lastChapterTitle = data_value.get('lastChapterTitle')
            book_info.lastChapterIndex = len(data_value.get('directoryInfo')) - 1
            book_info.lastPublishTime = data_value.get('lastPublishTime')
            book_info.tags = []
            for tag in data_value.get('bookTags'):
                book_info.tags.append(BOOK_TAGS[tag])
            book_info.wordCount = data_value.get('wordCount')
            book_info.bookAbstract = data_value.get('bookAbstract')
            book_info_items[book_info.bookId] = book_info.__dict__
        sort_history_book = []
        for book_id in book_id_list:
            if book_id in book_info_items:
                sort_history_book.append(book_info_items[book_id])
        return True, sort_history_book

    def get_collection_book(self):
        oms_client = OmsClient(OMS_RETRIEVE_URL, OMS_NAMESPACE_USER_BOOKCASE)
        try:
            result = oms_client.get_first_value(self.user_id)
        except:
            return []
        if not result:
            return []
        bookcase = result.get('dataValue').get('bookcase')
        return bookcase


    def get_history_book(self):
        oms_client = OmsClient(OMS_RETRIEVE_URL, OMS_NAMESPACE_USER_READ_BOOK_INFO)
        try:
            result = oms_client.get_first_value(self.user_id)
        except:
            return False, {'error': '获取浏览历史数据失败'}
        if not result:
            return False, {
                'data': {
                    'totalSize': 0,
                    'bookInfoItems': []
                }
            }
        self.total_size = len(result.get('dataValue').get('viewHistory'))
        if self.page_size + self.page_from < self.total_size:
            view_history = result['dataValue']['viewHistory'][self.page_from: self.page_from + self.page_size]
        else:
            view_history = result['dataValue']['viewHistory'][self.page_from:]
        print(len(view_history))
        return True, view_history

if __name__ == '__main__':
    param = {
        'userId': 'a87caac1-f14d-4372-9b3e-4bb837902f4d',
        'pageFrom': 0,
        'pageSize': 10
    }
    print(GetMyHistory(param).build_response())
