from swagger_server.api_providers.data_provider import DataProvider
from swagger_server.utils.oms_client import OmsClient
from swagger_server.constants import *

class ResponseModel:
    def __init__(self):
        self.bookName = ''
        self.content = ''
        self.authorSpeak = ''
        self.chapterTitle = ''
        self.titleIndex = ''
        self.contentWordCount = ''

class GetChapterContent(DataProvider):
    def __init__(self, params):
        DataProvider.__init__(self, params)
        self.book_id = params.get("bookId")
        self.item_id = params.get("itemId")

    def build_response(self):
        oms_client = OmsClient(OMS_RETRIEVE_URL, OMS_NAMESPACE_BOOK_PROFILE)
        try:
            book_profile = oms_client.get_first_value("regex::.*#{}.*".format(self.book_id))
        except:
            return {'error': '获取书籍信息失败'}
        data_value = book_profile.get('dataValue')
        response_data = ResponseModel()
        response_data.bookName = data_value.get("bookName")
        directory_info = data_value.get('directoryInfo')
        for item in directory_info:
            if item["itemId"] == self.item_id:
                response_data.chapterTitle = item['chapterTitle']
                response_data.titleIndex = item['titleIndex']
                break
        oms_client = OmsClient(OMS_RETRIEVE_URL, OMS_NAMESPACE_BOOK_CONTENT)
        try:
            content_profile = oms_client.get_first_value(self.item_id)
        except:
            return {'error': '获取文章内容失败'}
        data_value = content_profile.get('dataValue')
        response_data.content = data_value.get('content')
        response_data.authorSpeak = data_value.get('authorSpeak')
        response_data.contentWordCount = data_value.get('contentWordCount')
        return {'data': response_data.__dict__}