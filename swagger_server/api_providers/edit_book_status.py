from swagger_server.api_providers.data_provider import DataProvider
from swagger_server.utils.oms_client import OmsClient
from swagger_server.constants import *

class EditBookStatus(DataProvider):
    def __init__(self, params):
        DataProvider.__init__(self, params)
        self.book_id = params.get('bookId')
        self.status = params.get('status')

    def build_response(self):
        retrieve_oms_client = OmsClient(OMS_RETRIEVE_URL, OMS_NAMESPACE_BOOK_PROFILE)
        data_key = 'regex::.*#{}.*'.format(self.book_id)
        try:
            result = retrieve_oms_client.get_first_value(data_key)
        except:
            return {'error': '编辑失败，请重新操作'}
        if not result:
            return {'error': '查找书籍信息失败'}
        data_value = result.get('dataValue')
        data_key = result.get('dataKey')
        data_value['bookStatus'] = self.status
        update_oms_client = OmsClient(OMS_UPDATE_URL, OMS_NAMESPACE_BOOK_PROFILE)
        try:
            update_oms_client.update(data_key, data_value)
        except:
            return {'error': '编辑失败，请重新操作'}
        return {'data': 'success'}
