from swagger_server.api_providers.data_provider import DataProvider

class Test(DataProvider):
    def __init__(self, param_dict):
        DataProvider.__init__(self, param_dict)
        self.num1 = param_dict.get("numOne")
        self.num2 = param_dict.get("numTwo")

    def build_response(self):
        return {'data': self.num1 + self.num2}
