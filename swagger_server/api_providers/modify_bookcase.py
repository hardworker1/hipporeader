from swagger_server.api_providers.data_provider import DataProvider
from swagger_server.utils.oms_client import OmsClient
from swagger_server.constants import *

class ModifyBookcase(DataProvider):
    def __init__(self, param_dict):
        DataProvider.__init__(self, param_dict)
        self.user_id = param_dict.get('userId')
        self.book_id = param_dict.get("bookId")
        self.type = param_dict.get("type")

    def build_response(self):
        retrieve_oms_client = OmsClient(OMS_RETRIEVE_URL, OMS_NAMESPACE_USER_BOOKCASE)
        try:
            result = retrieve_oms_client.get_first_value(self.user_id)
        except:
            if self.type:
                return {'error': "添加失败，请重新操作"}
            return {'error': '删除失败，请重新操作'}
        if self.type:
            if not result:
                create_oms_client = OmsClient(OMS_CREATE_URL, OMS_NAMESPACE_USER_BOOKCASE)
                data_value = {
                    "bookcase": [self.book_id]
                }
                try:
                    create_oms_client.create(self.user_id, data_value)
                except:
                    return {'error': "添加失败，请重新操作"}
            else:
                update_oms_client = OmsClient(OMS_UPDATE_URL, OMS_NAMESPACE_USER_BOOKCASE)
                data_value = result.get('dataValue')
                if self.book_id not in data_value.get('bookcase'):
                    bookcase = [self.book_id]
                    bookcase.extend((data_value['bookcase']))
                    data_value['bookcase'] = bookcase
                    try:
                        update_oms_client.update(self.user_id, data_value)
                    except:
                        return {'error': "添加失败，请重新操作"}
        else:
            update_oms_client = OmsClient(OMS_UPDATE_URL, OMS_NAMESPACE_USER_BOOKCASE)
            data_value = result.get('dataValue')
            for i in range(len(data_value.get('bookcase'))):
                if self.book_id == data_value['bookcase'][i]:
                    data_value['bookcase'].pop(i)
                    try:
                        update_oms_client.update(self.user_id, data_value)
                    except:
                        return {'error': "删除失败，请重新操作"}
                    break
        return {'data': 'success'}

