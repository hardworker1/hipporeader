from swagger_server.utils.oms_client import OmsClient
from swagger_server.api_providers.data_provider import DataProvider
from swagger_server.constants import *


class BookInfoModel:
    def __init__(self):
        self.bookName = ''
        self.bookId = ''
        self.bookImg = ''
        self.bookAbstract = ''
        self.lastChapterTitle = ''
        self.lastPublishTime = ''
        self.lastChapterIndex = ''
        self.bookStatus = ''
        self.tags = []
        self.wordCount = 0

class GetAuthorInfo(DataProvider):
    def __init__(self, param):
        DataProvider.__init__(self, param)
        self.author_id = param.get('authorId')
        self.user_id = param.get('userId')

    def build_response(self):
        status, author_info, book_info_items = self.get_response_data()
        if not status:
            return author_info
        return {
            'data': {
                'authorInfo': author_info,
                'bookInfoItems': book_info_items
            }
        }

    def get_response_data(self):
        oms_client = OmsClient(OMS_RETRIEVE_URL, OMS_NAMESPACE_USER_PROFILE)
        try:
            result = oms_client.get_first_value('regex::^{}.*'.format(self.author_id))
        except:
            return False, {'error': '获取作者信息失败'}
        data_value = result.get('dataValue')
        author_info = {
            'authorName': data_value.get('userName'),
            'authorImg': data_value.get('image'),
            'authorDesc': data_value.get('description'),
            'authorId': data_value.get('userId'),
        }
        author_book = data_value.get('bookId')
        author_info['productionNum'] = len(author_book)
        status, total_word_count, book_info_items = self.get_book_word_count(author_book)
        if not status:
            return False, total_word_count, []
        author_info['wordCount'] = total_word_count
        return True, author_info, book_info_items

    def get_book_word_count(self, book_id_list):
        oms_client = OmsClient(OMS_RETRIEVE_URL, OMS_NAMESPACE_BOOK_PROFILE)
        book_id_str = '|'.join(book_id_list)
        data_key = 'regex::.*#({}).*'.format(book_id_str)
        try:
            result = oms_client.get_all_values(data_key)
        except:
            return False, {'error': '获取作者信息失败'}, []
        items = result.get('items')
        total_word_count = 0
        book_info_items = []
        for item in items:
            book_info = BookInfoModel()
            data_value = item.get('dataValue')
            total_word_count += data_value.get('wordCount')
            book_info.bookId = data_value.get('bookId')
            book_info.bookImg = data_value.get('bookImg')
            book_info.bookName = data_value.get('bookName')
            book_info.bookStatus = BOOK_STATUS[data_value.get('bookStatus')]
            book_info.lastChapterTitle = data_value.get('lastChapterTitle')
            book_info.lastChapterIndex = len(data_value.get('directoryInfo')) - 1
            book_info.lastPublishTime = data_value.get('lastPublishTime')
            book_info.tags = []
            for tag in data_value.get('bookTags'):
                book_info.tags.append(BOOK_TAGS[tag])
            book_info.wordCount = data_value.get('wordCount')
            book_info.bookAbstract = data_value.get('bookAbstract')
            book_info_items.append(book_info.__dict__)
        return True, total_word_count, book_info_items

if __name__ == '__main__':
    param = {'userId': 'a87caac1-f14d-4372-9b3e-4bb837902f4d', 'authorId': 'ce25ed55-58b7-4ad0-b31c-a7e164516351'}
    print(GetAuthorInfo(param).build_response())