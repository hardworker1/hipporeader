from swagger_server.api_providers.data_provider import DataProvider
from swagger_server.utils.oms_client import OmsClient
from swagger_server.constants import *

class SetUserReadBookInfo(DataProvider):
    def __init__(self, params_dict):
        DataProvider.__init__(self, params_dict)
        self.user_id = params_dict.get("userId")
        self.book_id = params_dict.get('bookId')
        self.index = params_dict.get('index')

    def build_response(self):
        oms_client = OmsClient(OMS_RETRIEVE_URL, OMS_NAMESPACE_USER_READ_BOOK_INFO)
        try:
            result = oms_client.get_first_value(self.user_id)
        except:
            return {'data': {'error': '保存信息失败'}}
        if not result:
            create_oms_client = OmsClient(OMS_CREATE_URL, OMS_NAMESPACE_USER_READ_BOOK_INFO)
            try:
                data_value = {
                    'viewHistory': [self.book_id],
                    'bookIndex': {
                        self.book_id: self.index
                    }
                }
                create_oms_client.create(self.user_id, data_value)
                return {'data': 'success'}
            except:
                return {'data': {'error': '保存用户信息失败'}}
        data_value = result.get('dataValue')
        data_value['bookIndex'][self.book_id] = self.index
        view_history = [self.book_id]
        if self.book_id not in data_value.get('viewHistory'):
            view_history.extend(data_value.get('viewHistory'))
            data_value['viewHistory'] = view_history
        else:
            for index in range(len(data_value.get('viewHistory'))):
                if data_value['viewHistory'][index] == self.book_id:
                    data_value['viewHistory'].pop(index)
                    break
            view_history.extend(data_value.get('viewHistory'))
            data_value['viewHistory'] = view_history
        update_oms_client = OmsClient(OMS_UPDATE_URL, OMS_NAMESPACE_USER_READ_BOOK_INFO)
        try:
            update_oms_client.update(self.user_id, data_value)
        except:
            return {'data': {'error': '保存用户信息失败'}}
        return {'data': 'success'}

if __name__ == '__main__':
    parma = {
        'userId': 'a87caac1-f14d-4372-9b3e-4bb837902f4d',
        'bookId': '6846366104005118983',
        'index': 5
    }
    SetUserReadBookInfo(parma).build_response()