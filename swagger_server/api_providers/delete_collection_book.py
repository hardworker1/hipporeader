from swagger_server.api_providers.data_provider import DataProvider
from swagger_server.utils.oms_client import OmsClient
from swagger_server.constants import *

class DeleteCollectionBook(DataProvider):
    def __init__(self, param_dict):
        DataProvider.__init__(self, param_dict)
        self.user_id = param_dict.get("userId")
        self.book_id = param_dict.get('bookId')

    def build_response(self):
        oms_client = OmsClient(OMS_RETRIEVE_URL, OMS_NAMESPACE_USER_BOOKCASE)
        try:
            result = oms_client.get_first_value(self.user_id)
        except:
            return {"error": "get user profile failed, userId={}".format(self.user_id)}
        if not result:
            return {'error': '删除失败，请重新操作'}
        data_value = result.get('dataValue')
        bookcase = data_value.get('bookcase')
        for i in range(len(bookcase)):
            if self.book_id == bookcase[i]:
                bookcase.pop(i)
                break
        data_value = {
            'bookcase': bookcase
        }
        update_oms_client = OmsClient(OMS_UPDATE_URL, OMS_NAMESPACE_USER_BOOKCASE)
        try:
            update_oms_client.update(self.user_id, data_value)
        except:
            return {'error': '删除失败，请重新操作'}
        return {'data': 'success'}