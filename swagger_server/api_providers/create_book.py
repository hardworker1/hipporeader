from swagger_server.api_providers.data_provider import DataProvider
from swagger_server.utils.oms_client import OmsClient
from swagger_server.constants import *
import random
import json

class CreateBook(DataProvider):
    def __init__(self, params):
        DataProvider.__init__(self, params)
        self.book_name = params.get('bookName')
        self.book_img = params.get('bookImg')
        self.tags = params.get('tags')
        self.description = params.get('description')
        self.object = params.get('object')
        self.user_id = params.get('userId')
        self.object = params.get('object')
        self.book_id = None

    def build_response(self):
        book_id = self.generate_book_id()
        self.book_id = book_id
        status, user_name = self.get_user_info()
        if not status:
            return user_name
        oms_client = OmsClient(OMS_CREATE_URL, OMS_NAMESPACE_BOOK_PROFILE)
        data_key = self.user_id + '#' + book_id
        self.tags = json.loads(self.tags)
        for tag in self.tags:
            data_key = data_key + '#' + str(tag)
        data_value = {
            'authorId': self.user_id,
            'authorName': user_name,
            'bookAbstract': self.description,
            'bookId': book_id,
            'bookName': self.book_name,
            'bookImg': self.book_img,
            'bookStatus': 2,
            'bookTags': self.tags,
            'directoryInfo': [],
            "lastChapterTitle": "",
            "lastPublishTime": "",
            "readerCount": 0,
            "wordCount": 0,
            "object": self.object,
        }
        try:
            oms_client.create(data_key, data_value)
        except:
            return {'error': '创建书籍失败，请重新操作'}
        return {'data': 'success'}

    def generate_book_id(self):
        book_id = ''
        for i in range(19):
            book_id += str(random.randint(0, 9))
        return book_id

    def get_user_info(self):
        oms_client = OmsClient(OMS_RETRIEVE_URL, OMS_NAMESPACE_USER_PROFILE)
        try:
            user_info = oms_client.get_first_value('regex::^{}.*'.format(self.user_id))
        except:
            return False, {'error': '创建书籍失败，请重新操作'}
        if not user_info:
            return False, {'error': '未查找到用户信息，请退出后重新操登入'}
        data_value = user_info.get('dataValue')
        if 'bookId' in data_value:
            data_value['bookId'].append(self.book_id)
        else:
            data_value['bookId'] = [self.book_id]
        data_key = user_info.get('dataKey')
        update_oms_client = OmsClient(OMS_UPDATE_URL, OMS_NAMESPACE_USER_PROFILE)
        try:
            update_oms_client.update(data_key, data_value)
        except:
            return False, {'error': '创建书籍失败，请重新操作'}
        user_name = user_info.get('dataValue').get('userName')
        return True, user_name