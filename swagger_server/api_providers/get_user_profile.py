from swagger_server.utils.oms_client import OmsClient
from swagger_server.api_providers.data_provider import DataProvider
from swagger_server.constants import *

class GetUserProfile(DataProvider):
    def __init__(self, param_dict):
        DataProvider.__init__(self, param_dict)
        self.user_id = param_dict.get("userId")

    def build_response(self):
        oms_client = OmsClient(OMS_RETRIEVE_URL, OMS_NAMESPACE_USER_PROFILE)
        try:
            data_key = "regex::{}.*".format(self.user_id)
            user_profile = oms_client.get_first_value(data_key)
        except:
            return {"error": "get user profile failed, userId={}".format(self.user_id)}
        if user_profile and user_profile.get("dataValue"):
            data_value = user_profile.get("dataValue")
            if data_value.get('password'):
                del data_value['password']
            return {"data": data_value}
        return {"data": {}}


