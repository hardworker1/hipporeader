from swagger_server.api_providers.data_provider import DataProvider
from swagger_server.utils.oms_client import OmsClient
from swagger_server.constants import *
import random
import datetime

class CacheContent(DataProvider):
    def __init__(self, params):
        DataProvider.__init__(self, params)
        self.book_id = params.get("bookId")
        self.chapter_title = params.get("chapterTitle")
        self.content = params.get("content")
        self.type = params.get("type")
        self.content_word_content = params.get("contentWordCount")
        self.author_speak = params.get('authorSpeak')
        self.title_index = params.get('titleIndex')

    def generate_title_id(self):
        content_id = ''
        for i in range(16):
            content_id += str(random.randint(0, 9))
        return content_id

    def build_response(self):
        oms_client = OmsClient(OMS_RETRIEVE_URL, OMS_NAMESPACE_BOOK_PROFILE)
        data_key = "regex::.*#{}.*".format(self.book_id)
        try:
            result = oms_client.get_first_value(data_key)
        except:
            return {'error': '从数据库中获取数据信息失败'}
        data_value = result.get("dataValue")
        directory_info = data_value.get("directoryInfo")
        item_id = self.generate_title_id()
        item = {
            "itemId": item_id,
            "type": self.type,
            "title": '第' + str(self.title_index) + '章 ' + self.chapter_title ,
            "titleIndex": self.title_index,
            "chapterTitle": self.chapter_title 
        }
        data_key = result.get('dataKey')
        directory_info.append(item)
        data_value['directoryInfo'] = directory_info
        if self.type == 1:
            data_value['lastPublishTime'] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            data_value['wordCount'] = data_value['wordCount'] + self.content_word_content
            data_value['lastChapterTitle'] = '第' + str(self.title_index) + '章 ' + self.chapter_title
        oms_client = OmsClient(OMS_UPDATE_URL, OMS_NAMESPACE_BOOK_PROFILE)
        try:
            oms_client.update(data_key, data_value)
        except:
            return {'error': '保存新章节失败'}
        data_value = {
            "content": self.content,
            "contentUpdateTime": datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
            "contentWordCount": self.content_word_content,
            "authorSpeak": self.author_speak,
            "itemId": item_id
        }
        oms_client = OmsClient(OMS_CREATE_URL, OMS_NAMESPACE_BOOK_CONTENT)
        try:
            oms_client.create(item_id, data_value)
        except:
            return {'error': '保存新章节失败'}
        return {'data': {'itemId': item_id}}