import connexion
import six
from swagger_server.models.console import Console  # noqa: E501
from swagger_server.models.console_response import ConsoleResponse  # noqa: E501
from swagger_server import util
from swagger_server.controllers.response_provider import ResponseProvider

def console(console_request):  # noqa: E501
    """console
     # noqa: E501
    :param console: 
    :type console: dict | bytes
    :rtype: ConsoleResponse
    """
    if not connexion.request.is_json:
        raise Exception("invalid parameter")
    request_json = connexion.request.get_json() # noqa: E501
    return ResponseProvider(request_json).build_response()
