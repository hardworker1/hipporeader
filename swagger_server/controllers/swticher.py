from swagger_server.api_providers.test import Test
from swagger_server.api_providers.get_book_info import GetBookInfo
from swagger_server.api_providers.get_book_detail import GetBookDetail
from swagger_server.api_providers.get_user_profile import GetUserProfile
from swagger_server.api_providers.get_book_content import GetBookContent
from swagger_server.api_providers.update_user_profile import UpdateUserProfile
from swagger_server.api_providers.user_read_book_info import UserReadBookInfo
from swagger_server.api_providers.set_user_read_book_info import SetUserReadBookInfo
from swagger_server.api_providers.subscription_author import SubscriptionAuthor
from swagger_server.api_providers.subscription_status import SubscriptionStatus
from swagger_server.api_providers.modify_bookcase import ModifyBookcase
from swagger_server.api_providers.collection_status import CollectionStatus
from swagger_server.api_providers.get_bookstore_data import GetBookstoreData
from swagger_server.api_providers.get_collection_book import GetCollectionBook
from swagger_server.api_providers.delete_collection_book import DeleteCollectionBook
from swagger_server.api_providers.get_author_info import GetAuthorInfo
from swagger_server.api_providers.get_my_history import GetMyHistory
from swagger_server.api_providers.get_my_subscription import GetMySubscription
from swagger_server.api_providers.create_book import CreateBook
from swagger_server.api_providers.get_user_book import GetUserBook
from swagger_server.api_providers.edit_book import EditBook
from swagger_server.api_providers.edit_book_status import EditBookStatus
from swagger_server.api_providers.cache_content import CacheContent
from swagger_server.api_providers.get_chapter import GetChapter
from swagger_server.api_providers.delete_chapter import DeleteChapter
from swagger_server.api_providers.get_chapter_content import GetChapterContent
from swagger_server.api_providers.update_chapter_content import UpdateChapterContent
from swagger_server.api_providers.get_sort_book import GetSortBook
from swagger_server.api_providers.search import Search


def switcher(command_type, param_dict):
    switch = {
        "$test": Test(param_dict),
        "$get-book-info": GetBookInfo(param_dict),
        "$get-book-detail": GetBookDetail(param_dict),
        "$get-user-profile": GetUserProfile(param_dict),
        "$get-book-content": GetBookContent(param_dict),
        "$update-user-profile": UpdateUserProfile(param_dict),
        "$user-read-book-info": UserReadBookInfo(param_dict),
        "$set-user-read-book-info": SetUserReadBookInfo(param_dict),
        "$subscription-author": SubscriptionAuthor(param_dict),
        "$subscription-status": SubscriptionStatus(param_dict),
        "$modify-bookcase": ModifyBookcase(param_dict),
        "$collection-status": CollectionStatus(param_dict),
        "$get-bookstore-data": GetBookstoreData(param_dict),
        "$get-collection-book": GetCollectionBook(param_dict),
        "$delete-collection-book": DeleteCollectionBook(param_dict),
        "$get-author-info": GetAuthorInfo(param_dict),
        "$get-my-history": GetMyHistory(param_dict),
        "$get-my-subscription": GetMySubscription(param_dict),
        "$create-book": CreateBook(param_dict),
        "$get-user-book": GetUserBook(param_dict),
        "$edit-book": EditBook(param_dict),
        "$edit-book-status": EditBookStatus(param_dict),
        "$cache-content": CacheContent(param_dict),
        "$get-chapter":GetChapter(param_dict),
        "$delete-chapter": DeleteChapter(param_dict),
        "$get-chapter-content": GetChapterContent(param_dict),
        "$update-chapter-content": UpdateChapterContent(param_dict),
        "$get-sort-book": GetSortBook(param_dict),
        "$search": Search(param_dict),
    }
    return switch[command_type].build_response()