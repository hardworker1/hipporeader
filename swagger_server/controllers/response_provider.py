import shlex
from swagger_server.argparse_models.argparse_switch import *
from swagger_server.controllers.swticher import *


class ResponseProvider:
    def __init__(self, param_dict):
        self.param_dict = param_dict

    def build_response(self):
        command_type, new_param = self.get_new_param()
        try:
            argparse = argparse_switch(command_type)
            command_param = argparse.parse_args(shlex.split(new_param)).__dict__
        except KeyError as e:
            raise {"error": "invalid parameter"}
        command_response = switcher(command_type, command_param)
        return command_response

    def get_new_param(self):
        param = self.param_dict.get("data").get("command")
        command_type = ""
        new_param_dict = ""
        for index in range(len(param)):
            if index == 0 and param[index] != "$":
                return {"error": "invalid parameter"}
            if param[index] == ' ':
                if index != len(param) - 1:
                    if param[index + 1] == ' ':
                        new_param_dict = param[index+1:]
                    else:
                        new_param_dict = param[index:]
                break
            command_type += param[index]
        return command_type, new_param_dict