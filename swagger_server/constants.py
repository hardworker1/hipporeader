import socket
local_ip = socket.gethostbyname(socket.gethostname())

OMS_PORT = '8081'
HIPPO_PORT = '8080'
HIPPO_ADMIN_PORT = '4444'
HIPPO_UX_PORT = '8082'

OMS_URL = "http://" + local_ip + ":" + OMS_PORT + "/oms/v1"
OMS_RETRIEVE_URL = OMS_URL + "/items"
OMS_CREATE_URL = OMS_URL + "/item"
OMS_DELETE_URL = OMS_URL + "/items"
OMS_UPDATE_URL = OMS_URL + "/items"

OMS_AUTHORIZATION = 'Bearer mi-oms'

# oms namespace
OMS_NAMESPACE_USER_PROFILE = 'oms-user-profile'
OMS_NAMESPACE_BOOK_PROFILE = 'book-profile'
OMS_NAMESPACE_BOOK_CONTENT = 'book-content'
OMS_NAMESPACE_USER_READ_HISTORY = 'user-read-history'
OMS_NAMESPACE_USER_BOOKCASE = 'bookcase'
OMS_NAMESPACE_USER_READ_BOOK_INFO = 'user-read-book-info'

# book tags
XUANHUAN = 1
DU_SHI = 2
CHONGSHENG = 3
CHUANYUE = 4
XITONG = 5
YANQING = 6
BOOK_TAGS_VALUE = {"玄幻": XUANHUAN, "都市": DU_SHI,"重生": CHONGSHENG, '言情': YANQING, '穿越': CHUANYUE, '系统': XITONG, '现代言情': YANQING, '古代言情': YANQING}
BOOK_TAGS = {XUANHUAN: "玄幻", DU_SHI: "都市", CHONGSHENG: "重生", YANQING: '言情', CHUANYUE: '穿越', XITONG: '系统'}

# book status
FINISHED = 1
SERIAL = 2
BOOK_STATUS_VALUE = {"连载中": SERIAL, "已完结": FINISHED}
BOOK_STATUS = {SERIAL: "连载中", FINISHED: "已完结"}

# bookstore sort by
SORT_BY_HOTTEST = 1
SORT_BY_LATEST = 2
SORT_BY_WORD_COUNT = 3
SORT_BY = {SORT_BY_HOTTEST: 'readerCount', SORT_BY_LATEST: 'lastPublishTime', SORT_BY_WORD_COUNT: 'wordCount'}

# chapter status
CHAPTER_PUBLISH = 1
CHAPTER_NOT_PUBLISH = 2
CHAPTER_STATUS = {CHAPTER_PUBLISH: '已发表', CHAPTER_NOT_PUBLISH: '未发表'}


