# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.console import Console  # noqa: E501
from swagger_server.models.console_response import ConsoleResponse  # noqa: E501
from swagger_server.test import BaseTestCase


class TestHippoController(BaseTestCase):
    """HippoController integration test stubs"""

    def test_console(self):
        """Test case for console

        
        """
        console = Console()
        response = self.client.open(
            '/hippo/v1/console',
            method='POST',
            data=json.dumps(console),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
